/*
 * CraftBook Copyright (C) 2010-2023 sk89q <http://www.sk89q.com>
 * CraftBook Copyright (C) 2011-2023 me4502 <http://www.me4502.com>
 * CraftBook Copyright (C) Contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not,
 * see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.util.parsing.block.state;

import com.minecraftonline.util.IDUtil;
import com.minecraftonline.util.parsing.lineparser.Restriction;
import com.sk89q.craftbook.sponge.InvalidICException;
import com.sk89q.craftbook.sponge.mechanics.ics.ICSocket;
import com.sk89q.craftbook.sponge.util.BlockUtil;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.api.block.BlockState;

public class BlockStateRestrictions {

    public interface BlockStateRestriction extends Restriction<BlockState> {}

    public static final BlockStateRestriction HAS_ITEM_FORM = new BlockStateRestriction() {
        @Override
        public void apply(BlockState target) throws InvalidICException {
            if (IDUtil.getItemStackFromBlockState(target).isEmpty()) {
                throw new InvalidICException("Block must have an item form");
            }
        }

        @Nullable
        @Override
        public String getDescription() {
            return "Block must have an item form";
        }
    };

    public static final BlockStateRestriction ALLOWED_IC_BLOCK = new BlockStateRestriction() {
        @Override
        public void apply(BlockState target) throws InvalidICException {
            if (!BlockUtil.doesStatePassFilters(ICSocket.allowedICBlocks.getValue(), target)) {
                throw new InvalidICException("Block: '" + target.getId() + "' is not allowed.");
            }
        }

        @Nullable
        @Override
        public String getDescription() {
            return "Block must be allowed by the server's config.";
        }
    };
}
